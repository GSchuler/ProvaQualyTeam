﻿using QualyTeam.WebSite.QualyTeamService;
using QualyTeam.WebSite.ViewModels;
using QualyTeam.Service.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QualyTeam.WebSite.Controllers
{
    public class DocumentController : Controller
    {
        private readonly ServiceClient _service;

        public DocumentController()
        {
            _service = new ServiceClient();
        }

        public ActionResult Index()
        {
            return RedirectToAction("DocumentsList");
        }

        public ActionResult DocumentsList(string message = "", int status = 0, string filterTag = "")
        {
            if (status != 0)
            {
                ViewBag.FeedbackMensagem = message;
                if (status == (int)FeedbackStatus.Success)
                    ViewBag.FeedbackStatus = "Sucesso";
                if (status == (int)FeedbackStatus.Fail)
                    ViewBag.FeedbackStatus = "Operação cancelada";
            }

            var documents = _service.ListDocuments();

            switch (filterTag)
            {
                case "codigo":
                    documents = documents.OrderBy(d => d.Codigo).ToList();
                    break;
                case "processo":
                    documents = documents.OrderBy(d => d.Processo).ToList();
                    break;
                case "categoria":
                    documents = documents.OrderBy(d => d.Categoria).ToList();
                    break;
                default:
                    documents = documents.OrderBy(d => d.Titulo).ToList();
                    break;
            }
            return View(documents.ConvertAll(d => d.ToViewModel()));
        }

        public ActionResult Save(string message = "", int status = 0)
        {
            if (status != 0)
            {
                ViewBag.FeedbackMensagem = message;
                if (status == (int)FeedbackStatus.Success)
                    ViewBag.FeedbackStatus = "Sucesso";
                if (status == (int)FeedbackStatus.Fail)
                    ViewBag.FeedbackStatus = "Operação cancelada";
            }
            return View(new DocumentViewModel());
        }

        [HttpPost]
        public ActionResult Save(DocumentViewModel vm)
        {
            if (ModelState.IsValid)
            {
                if (_service.GetDocumentByCodigo(vm.Codigo) == null)
                {
                    var feedback = _service.SaveDocument(vm.ToDto());
                    if (feedback.Status == FeedbackStatus.Success)
                        return RedirectToAction("DocumentsList", new { message = feedback.Mensagem, status = (int)feedback.Status });
                    else
                        return RedirectToAction("Save", new { message = feedback.Mensagem, status = (int)feedback.Status });
                }
                else
                    return View(vm);
            }
            else
                return View(vm);
        }

        public FileResult DownloadFile(int codigo)
        {
            var document = _service.GetDocumentByCodigo(codigo).ToViewModel();
            return File(document.ArquivoByte, document.ArquivoExt, string.Format("Documento-Codigo{0}.pdf", document.Codigo.ToString()));
        }
    }
}