﻿using System;
using System.ComponentModel.DataAnnotations;
using QualyTeam.Service.Dto;
using System.Web;
using System.Collections.Generic;
using QualyTeam.WebSite.Helper;

namespace QualyTeam.WebSite.ViewModels
{
    [Serializable]
    public class DocumentViewModel
    {
        [Display(Name = "Codigo", Prompt = "Código", Order = 0)]
        [Required(ErrorMessage = "O campo Código não é válido.")]
        [CodigoValidator]
        public int Codigo { get; set; }

        [Display(Name = "Titulo", Prompt = "Título", Order = 1)]
        [StringLength(255)]
        [Required(ErrorMessage = "O campo Título não é válido.")]
        public string Titulo { get; set; }

        [Display(Name = "Processo", Prompt = "Título", Order = 2)]
        [StringLength(255)]
        [Required(ErrorMessage = "O campo Processo não é válido.")]
        public string Processo { get; set; }

        [Display(Name = "Categoria", Prompt = "Categoria", Order = 3)]
        [StringLength(255)]
        [Required(ErrorMessage = "O campo Categoria não é válido.")]
        public string Categoria { get; set; }

        [Required][Helper.FileExtensionsAttribute("pdf,xls,doc", ErrorMessage = "Extensão do arquivo incorreta (extensões válidas: xls, doc, pdf).")]
        public HttpPostedFileBase Arquivo { get; set; }

        public byte[] ArquivoByte { get; set; }

        public string ArquivoExt { get; set; }

        public DocumentViewModel()
        {

        }

        public DocumentViewModel(DocumentDto dto)
        {
            Codigo = dto.Codigo;
            Titulo = dto.Titulo;
            Processo = dto.Processo;
            Categoria = dto.Categoria;
            ArquivoByte = dto.Arquivo;
            ArquivoExt = dto.ArquivoExt;
        }
    }

    public static class DocumentViewModelMapper
    {
        public static DocumentViewModel ToViewModel(this DocumentDto dto)
        {
            return new DocumentViewModel(dto);
        }
        public static DocumentDto ToDto(this DocumentViewModel dvm)
        {
            var extDvm="";
            bool ext;
            if (dvm.Arquivo != null)
                ext = _mappings.TryGetValue(dvm.Arquivo.ContentType, out extDvm);
            return new DocumentDto
            {
                Titulo = dvm.Titulo,
                Codigo = dvm.Codigo,
                Arquivo = (dvm.Arquivo != null && extDvm != "") ? Helper.Util.FileToByteArray(dvm.Arquivo) : null,
                Categoria = dvm.Categoria,
                Processo = dvm.Processo,
                ArquivoExt = extDvm
            };
        }

        private static IDictionary<string, string> _mappings = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
        {
            {"application/vnd.ms-excel", "xls"},
            {"application/msword", "doc"},
            {"application/pdf", "pdf"}
        };
    }
}