﻿using System.Web.Mvc;
using System.Web.Routing;

namespace QualyTeam.WebSite.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{filterTag}",
                defaults: new { controller = "Document", action = "Index", filterTag = UrlParameter.Optional }
            );
        }
    }
}