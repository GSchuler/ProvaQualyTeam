﻿using System;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace QualyTeam.WebSite.Helper
{
    public static class InputHelper
    {
        public static IHtmlString InputTextBoxWithLabel<T, TValue>(this HtmlHelper<T> htmlHelper, Expression<Func<T, TValue>> function)
        {
            return new MvcHtmlString(@"" +
             @"<section>" +
               htmlHelper.LabelFor(function) +
               @"<div>" +
                   htmlHelper.TextBoxFor(function, new { @class = "text" }) +
                   htmlHelper.ValidationMessageFor(function, "", new { @class = "alert warning" }) +
                @"</div>
            </section>");
        }

        public static IHtmlString InputEditorWithLabel<T, TValue>(this HtmlHelper<T> htmlHelper, Expression<Func<T, TValue>> function)
        {
            return new MvcHtmlString(@"" +
             @"<section>" +
               htmlHelper.LabelFor(function) +
               @"<div>" +
                   htmlHelper.EditorFor(function, new { htmlAttributes = new { @class = "text" } }) +
                   htmlHelper.ValidationMessageFor(function, "", new { @class = "alert warning" }) +
                @"</div>
            </section>");
        }

        public static IHtmlString DisplayEditorWithLabel<T, TValue>(this HtmlHelper<T> htmlHelper, Expression<Func<T, TValue>> function)
        {
            return new MvcHtmlString(@"" +
             @"<section>" +
               htmlHelper.LabelFor(function) +
               @"<div>" +
                   htmlHelper.DisplayFor(function) +
                @"</div>
            </section>");
        }
        
    }
}