﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QualyTeam.WebSite.Helper
{
    public class CodigoValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                int codigo = (int)value;

                if (codigo > 0)
                    return ValidationResult.Success;
            }
            return new ValidationResult("O campo Código não é válido");
        }
    }
}