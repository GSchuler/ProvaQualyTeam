﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace QualyTeam.WebSite.Helper
{
    public static class Util
    {
        public static byte[] FileToByteArray(HttpPostedFileBase file)
        {
            byte[] data = null;
            if (file != null)
            {
                using (Stream inputStream = file.InputStream)
                {
                    MemoryStream memoryStream = inputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        inputStream.CopyTo(memoryStream);
                    }
                    data = memoryStream.ToArray();
                }
            }
            return data;
        }
    }
}