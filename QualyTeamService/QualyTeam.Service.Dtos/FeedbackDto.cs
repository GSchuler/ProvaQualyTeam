﻿namespace QualyTeam.Service.Dto
{
    public class FeedBackDto
    {
        public FeedbackStatus Status { get; set; }
        public string Mensagem { get; set; }
    }
}
