﻿using System;
using System.Runtime.Serialization;

namespace QualyTeam.Service.Dto
{
    [Flags]
    public enum FeedbackStatus : int
    {
        [EnumMember]
        Success = 1,
        [EnumMember]
        Fail = 2
    }
}
