﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualyTeam.Service.Dto
{
    public class DocumentDto
    {
        public int Codigo { get; set; }
        public string Titulo { get; set; }
        public string Processo { get; set; }
        public string Categoria { get; set; }
        public byte[] Arquivo { get; set; }
        public string ArquivoExt { get; set; }
    }
}
