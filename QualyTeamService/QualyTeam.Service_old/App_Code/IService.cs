﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using QualyTeam.Service.Dto;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
[ServiceContract(Namespace = "QualyTeam.Service")]
public interface IService
{

    [OperationContract]
    FeedBackDto SaveDocument(DocumentDto document);
    
    [OperationContract]
    List<DocumentDto> ListDocuments();

    [OperationContract]
    List<DocumentDto> ListDocumentsByField(string fieldName);
}
