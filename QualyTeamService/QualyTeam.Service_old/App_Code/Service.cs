﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using QualyTeam.Service.Dto;
using QualyTeam.Service.Dao;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
[ServiceBehavior(Namespace = "QualyTeam.Service")]
public class Service : IService
{
    private FeedBackDto fb;
    private DocumentDto doc;
    private DocumentosDao dao;

    public Service()
    {
        fb = new FeedBackDto();
        doc = new DocumentDto();
        dao = new DocumentosDao();
    }

    public FeedBackDto SaveDocument(DocumentDto document)
    {
        doc = document;
        
        if (dao.Insert(doc))
        {
            fb.mensagem = "Dados inseridos com sucesso.";
            fb.status = FeedbackStatus.Success;
        }
        else
        {
            fb.mensagem = "Falha ao inserir os dados.";
            fb.status = FeedbackStatus.Fail;
        }

        return fb;
    }

    public List<DocumentDto> ListDocuments()
    {
        return dao.SelectAll().OrderBy(f => f.Titulo).ToList();
    }

    public List<DocumentDto> ListDocumentsByField(string fieldName)
    {
        var select = dao.SelectAll();
        switch (fieldName)
        {
            case "codigo":
                return select.OrderBy(f => f.Codigo).ToList();
            case "processo":
                return select.OrderBy(f => f.Processo).ToList();
            case "categoria":
                return select.OrderBy(f => f.Categoria).ToList();
            default:
                return select.OrderBy(f => f.Titulo).ToList();
        }
    }
}
