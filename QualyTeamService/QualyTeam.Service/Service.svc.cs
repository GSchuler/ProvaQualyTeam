﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using QualyTeam.Service.Dto;
using QualyTeam.Service.Dao;

namespace QualyTeam.Service
{
    [ServiceBehavior(Namespace = "QualyTeam.Service")]
    public class Service : IService
    {
        private FeedBackDto fb;
        private DocumentosDao dao;
        private List<string> extsAllowed;

        public Service()
        {
            fb = new FeedBackDto();
            dao = new DocumentosDao();
            extsAllowed = new List<string> { "xls", "pdf", "doc" };
        }

        private void FillFeedback(FeedbackStatus status, string msg = "", bool clear = false)
        {
            if (clear)
                fb.Mensagem = "";
            fb.Status = status;
            fb.Mensagem += msg;
        }

        private void ValidateDocument(DocumentDto document)
        {
            FillFeedback(FeedbackStatus.Success);

            if (document.Codigo < 1)
                FillFeedback(FeedbackStatus.Fail,
                        "1. O Código " + document.Codigo + " não é válido). ");
            else if (dao.SelectByCodigo(document.Codigo).Any())
                FillFeedback(FeedbackStatus.Fail,
                        "1. O Código " + document.Codigo + " já está sendo usado). ");

            if (string.IsNullOrEmpty(document.Titulo))
                FillFeedback(FeedbackStatus.Fail,
                    "2. Preencha o campo Titulo corretamente. ");

            if (string.IsNullOrEmpty(document.Processo))
                FillFeedback(FeedbackStatus.Fail,
                    "3. Preencha o campo Processo corretamente. ");

            if (string.IsNullOrEmpty(document.Categoria))
                FillFeedback(FeedbackStatus.Fail,
                    "4. Preencha o campo Categoria corretamente. ");


            if (document.Arquivo == null)
                FillFeedback(FeedbackStatus.Fail,
                    "5. Nenhum arquivo foi anexado ao documento (extensões válidas: xls, doc, pdf). ");
            else if (document.ArquivoExt == null)
                FillFeedback(FeedbackStatus.Fail,
                    "5. Extensão do arquivo incorreta (extensões válidas: xls, doc, pdf). ");
            else if (document.ArquivoExt != null)
                if (!extsAllowed.Any(ext => document.ArquivoExt.EndsWith(ext)))
                    FillFeedback(FeedbackStatus.Fail,
                        "5. Extensão do arquivo incorreta (extensões válidas: xls, doc, pdf). ");
        }

        public FeedBackDto SaveDocument(DocumentDto document)
        {
            ValidateDocument(document);

            if (fb.Status == FeedbackStatus.Success)
            {
                var insertion = dao.Insert(document);
                if (insertion.Status == FeedbackStatus.Success)
                    FillFeedback(insertion.Status, "Dados inseridos com sucesso. ", true);
                else
                    FillFeedback(insertion.Status, insertion.Mensagem, true);
            }

            return fb;
        }

        public List<DocumentDto> ListDocuments()
        {
            return dao.Select().OrderBy(f => f.Titulo).ToList();
        }

        public DocumentDto GetDocumentByCodigo(int codigo)
        {
            return dao.SelectByCodigo(codigo).FirstOrDefault();
        }
    }
}