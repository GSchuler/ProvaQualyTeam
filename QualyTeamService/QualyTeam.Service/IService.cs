﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using QualyTeam.Service.Dto;

namespace QualyTeam.Service
{
    [ServiceContract(Namespace = "QualyTeam.Service")]
    public interface IService
    {

        [OperationContract]
        FeedBackDto SaveDocument(DocumentDto document);

        [OperationContract]
        List<DocumentDto> ListDocuments();
        
        [OperationContract]
        DocumentDto GetDocumentByCodigo(int codigo);
    }
}