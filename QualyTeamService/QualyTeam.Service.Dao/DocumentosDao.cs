﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QualyTeam.Service.Dto;
using MySql.Data.MySqlClient;

namespace QualyTeam.Service.Dao
{
    public class DocumentosDao
    {
        private Connection cc;
        private List<string> fields;
        private string tableName;

        public DocumentosDao()
        {
            fields = new List<String>() { "Codigo", "Titulo", "Processo", "Categoria", "Arquivo", "extArquivo" };
            tableName = "Documentos";
            cc = new Connection();
        }

        public FeedBackDto Insert(DocumentDto d)
        {
            var fb = new FeedBackDto();
            try
            {
                if (cc.OpenConnection())
                {
                    var fieldContent = "";
                    fields.ForEach(f =>
                    {
                        fieldContent += f.ToString() + ", ";
                    });

                    fieldContent = fieldContent.Remove(fieldContent.Length - 2);

                    using (MySqlCommand command = new MySqlCommand())
                    {
                        command.Connection = cc.conn;

                        command.CommandText = string.Format("INSERT INTO {0} ({1}) VALUES (?codigo, ?titulo, ?processo, ?categoria, ?arquivo, ?extArquivo)", tableName, fieldContent);
                        command.Parameters.Add("?codigo", MySqlDbType.Int64).Value = d.Codigo;
                        command.Parameters.Add("?titulo", MySqlDbType.VarChar).Value = d.Titulo;
                        command.Parameters.Add("?processo", MySqlDbType.VarChar).Value = d.Processo;
                        command.Parameters.Add("?categoria", MySqlDbType.VarChar).Value = d.Categoria;
                        command.Parameters.Add("?arquivo", MySqlDbType.LongBlob).Value = d.Arquivo;
                        command.Parameters.Add("?extArquivo", MySqlDbType.VarChar).Value = d.ArquivoExt;

                        command.ExecuteNonQuery();

                        cc.CloseConnection();
                    }

                    fb.Status = FeedbackStatus.Success;
                    fb.Mensagem = "Dados inseridos com sucesso";

                    return fb;
                }

                fb.Status = FeedbackStatus.Fail;
                fb.Mensagem = "Não foi possível cadastrar os dados porque a conexão com o banco de dados não foi resolvida.";

                return fb;
            }
            catch (Exception ex)
            {
                if (cc.IsConnectionOpen())
                    cc.CloseConnection();
                System.Diagnostics.Debug.WriteLine("Nenhum dado foi cadastrado... ERRO: " + ex.Message);
                fb.Status = FeedbackStatus.Fail;
                fb.Mensagem = "Nenhum dado foi cadastrado... ERRO: " + ex.Message;

                return fb;
            }
        }

        public List<DocumentDto> Select(string whereParams = "")
        {
            var documents = new List<DocumentDto>();
            try
            {
                if (cc.OpenConnection())
                {
                    var query = string.Format("select * from {0} {1}", tableName, whereParams);
                    MySqlCommand cmd = new MySqlCommand(query, cc.conn);
                    using (var documentsReader = cmd.ExecuteReader())
                    {
                        while (documentsReader.Read())
                        {
                            documents.Add(new DocumentDto
                            {
                                Codigo = Convert.ToInt32(documentsReader["codigo"].ToString()),
                                Titulo = documentsReader["titulo"].ToString(),
                                Processo = documentsReader["processo"].ToString(),
                                Categoria = documentsReader["categoria"].ToString(),
                                Arquivo = (byte[])documentsReader["arquivo"],
                                ArquivoExt = documentsReader["extArquivo"].ToString()
                            });
                        }
                    };

                    cc.CloseConnection();
                    System.Diagnostics.Debug.WriteLine("Dados retornados com sucesso");
                }
            }
            catch (Exception ex)
            {
                if (cc.IsConnectionOpen() == true)
                    cc.CloseConnection();
                System.Diagnostics.Debug.WriteLine("Nenhum dado foi retornado... ERRO: " + ex.Message);
            }
            return documents;

        }

        public List<DocumentDto> SelectByCodigo(int codigo)
        {
            var dict = new Dictionary<string, string>();
            dict.Add("codigo", codigo.ToString());
            return SelectByField(dict);
        }

        private List<DocumentDto> SelectByField(IDictionary<string, string> dict)
        {
            var whereClause = "where ";

            foreach (var item in dict)
                whereClause += string.Format("{0} = '{1}', ", item.Key, item.Value);

            return Select(whereClause.Remove(whereClause.Length - 2));
        }
    }
}
