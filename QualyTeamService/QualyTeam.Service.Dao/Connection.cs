﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace QualyTeam.Service.Dao
{
    public class Connection
    {
        public MySqlConnection conn;
        private string connString = ConfigurationManager.AppSettings["connectionString"];

        public Connection()
        {
            Init();
        }
        private void Init()
        {
            conn = new MySqlConnection(connString);
        }
        public bool OpenConnection()
        {
            try
            {
                conn.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                System.Diagnostics.Debug.WriteLine("Erro: " + ex.Message);
                return false;

            }
        }
        public bool CloseConnection()
        {
            try
            {
                conn.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }
        }
        public bool IsConnectionOpen()
        {
            if (conn.State == System.Data.ConnectionState.Closed)
                return false;
            else
                return true;
        }
    }
}
