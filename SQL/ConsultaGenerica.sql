Drop table Documentos;

create table Documentos(
codigo bigint PRIMARY KEY,
titulo varchar(255) not null,
processo varchar(255) not null, 
categoria varchar(255) not null,
arquivo LONGBLOB not null,
extArquivo varchar(4) not null
);


insert into Documentos /*(codigo, titulo, processo, categoria, arquivo)*/
values (123321, 'Registro de Teste', 'Prova de proficiência', 'Provas aplicadas', LOAD_FILE('C:\\ProvaQualyTeam\\ProvaQualyTeam\\SQL\\Teste.pdf'), 'pdf'); 


select * from Documentos;